GO
CREATE VIEW JOB_ACCRL_XWALK
AS
SELECT
  tcp_job_cd.JobCodeId AS JOBCODE,
  tcp_acc_bank.BankId AS ACCBANK
FROM tcp_EmployeeWork.JobCode tcp_job_cd
JOIN tcp_EmployeeLeave.AccrualBankJobCode tcp_accrual_bnk_job_cd
  ON tcp_accrual_bnk_job_cd.JobCodeRecordId = tcp_job_cd.RecordId
JOIN tcp_EmployeeLeave.AccrualBank tcp_acc_bank
  ON tcp_acc_bank.RecordId = tcp_accrual_bnk_job_cd.AccrualBankRecordId;
--
GO
CREATE VIEW ALL_EMPL
AS
SELECT
  tcp_emp1.EmployeeId AS EMPL_ID
FROM tcp_Employee.Employee tcp_emp1;