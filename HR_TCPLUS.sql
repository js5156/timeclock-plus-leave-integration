create or replace package                hr_tcplus is

   PROCEDURE PROCESS_PAYROLL(strYear ptrcaln.ptrcaln_year%type,
                             strCode ptrcaln.ptrcaln_pict_code%type,
                             strPayNo ptrcaln.ptrcaln_payno%type);
   
   PROCEDURE TRANSFER_LEAVE_HOURS (fileName IN VARCHAR2 DEFAULT NULL); 
/*   PROCEDURE SYNC_EMPLOYEES; */
                                      

end;
/

create or replace package body                hr_tcplus is 

      PROCEDURE PROCESS_PAYROLL
                ( strYear     IN ptrcaln.ptrcaln_year%type,
                  strCode     IN ptrcaln.ptrcaln_pict_code%type,
                  strPayNo    IN ptrcaln.ptrcaln_payno%type)

      IS

      v_procedure util_errorlog.procedure_name%TYPE := 'PROCESS_PAYROLL';
      v_location util_errorlog.location%TYPE;


      calYear	         ptrcaln.ptrcaln_year%type;
      calPictCode	     ptrcaln.ptrcaln_pict_code%type;
      calPayNo         ptrcaln.ptrcaln_payno%type;
      calStartDate     ptrcaln.ptrcaln_start_date%type;
      calEndDate       ptrcaln.ptrcaln_end_date%type;
      emplPidm         spriden.spriden_pidm%type;
      emplTcid         spriden.spriden_id%type;
      emplPosn         nbrjobs.nbrjobs_posn%type;
      emplSuff         nbrjobs.nbrjobs_suff%type;
      emplEclsCode     nbrjobs.nbrjobs_ecls_code%type;
      emplEarnCode     ptrearn.ptrearn_code%type;
      emplWorkDate     phrmtim.phrmtim_begin_date%type;
      emplHrs          phrmtim.phrmtim_hrs%type;
      emplSalTable     nbrjobs.nbrjobs_sal_table%type;
      emplSalGrade     nbrjobs.nbrjobs_sal_grade%type;
      emplSpecialRate  phrmtim.phrmtim_special_rate%type;

      --Declare exception types
      invalid_pay_period exception;
      invalid_id exception;
      invalid_job exception;
      invalid_earn_code exception;
      invalid_supv_pay exception;


      cursor  curPayCalendar
      is	select  ptrcaln_year,
          ptrcaln_pict_code,
          ptrcaln_payno,
          ptrcaln_start_date,
          ptrcaln_end_date
        from ptrcaln
        where ptrcaln_year = strYear and
              ptrcaln_pict_code = strCode and
              ptrcaln_payno = strPayNo;

      recPayCalendar curPayCalendar%RowType;

      cursor  curEmplHoursTCP
      is       select
               badge_number,
               job_code,
               date_in,
               sum(TO_NUMBER(regular) +
               TO_NUMBER(overtime1) +
               TO_NUMBER(overtime2)) as hours
               from tcapp.hr_tcp_external
               GROUP BY badge_number, job_code, date_in
               order by badge_number,job_code;


      cursor  curEmplId (badge_number_in VARCHAR2)
      is        select
                spriden_pidm,
                spriden_id
                from spriden
                where spriden_id = 'T' || badge_number_in
                and spriden_change_ind is null;

      recEmplId curEmplId%RowType;

      cursor  curEmplJobRec (pidm_in NUMBER, begin_work_date_in DATE)
      is        select
                nbrjobs_posn,
                nbrjobs_suff,
                nbrjobs_ecls_code,
                nbrjobs_sal_table,
                nbrjobs_sal_grade
                from nbrjobs jd,nbrbjob
                where nbrjobs_pidm = pidm_in
                and nbrjobs_effective_date = (select max(nbrjobs_effective_date)
                                              from nbrjobs
                                              where nbrjobs_pidm = jd.nbrjobs_pidm
                                              and nbrjobs_posn = jd.nbrjobs_posn
                                              and nbrjobs_suff = jd.nbrjobs_suff
                                              and nbrjobs_effective_date <= begin_work_date_in)
                and nbrbjob_pidm = nbrbjob_pidm
                and nbrbjob_posn = nbrjobs_posn
                and nbrbjob_suff = nbrbjob_suff
                and nbrbjob_contract_type = 'P'
                and ((nbrjobs_status = 'T' and begin_work_date_in = nbrjobs_effective_date) or
                (nbrjobs_status = 'A'));

      recEmplJobRec curEmplJobRec%RowType;

      cursor curEarnXwalkLookup (job_code_in varchar2)
      is        select
                xwalk_earn_code
                from
                tcapp.hr_tcp_bann_earn_xwalk
                where xwalk_job_code = job_code_in;

      recEarnXwalkLookup curEarnXwalkLookup%RowType;


      cursor curSupervisorXwalkLookup (sal_table_in varchar2, sal_grade_in varchar2)
      is      select
              xwalk_sal_grade,
              xwalk_supv_grade,
              ntrsalb_high
              from tcapp.hr_tcp_bann_supervisor_xwalk,ftvfsyr,ntrsalb
              where xwalk_sal_table = sal_table_in
              and xwalk_sal_grade = sal_grade_in
              and xwalk_sal_table = ntrsalb_table (+)
              and xwalk_supv_grade = ntrsalb_grade (+)
              and ftvfsyr_start_date <= calStartDate
              and ftvfsyr_end_date >= calStartDate
              and (ntrsalb_sgrp_code = ftvfsyr_fsyr_code || 'SAL'
              or ntrsalb_sgrp_code is null);

      recSupervisorXwalkLookup curSupervisorXwalkLookup%RowType;

      cursor curPayrollStaging
      is     select
             *
             from tcapp.hr_tcp_bann_staging;

      BEGIN

      dbms_output.put_line (sysdate || ' hr_tcplus.tcp_bann_transfer - started');


       --Populate staging table with data
      OPEN curPayCalendar;

      FETCH curPayCalendar INTO recPayCalendar;

      IF(curPayCalendar%NOTFOUND) then
        CLOSE curPayCalendar;
        RAISE invalid_pay_period;
      end if;

      calYear := recPayCalendar.ptrcaln_year;
      calPictCode := recPayCalendar.ptrcaln_pict_code;
      calPayNo := recPayCalendar.ptrcaln_payno;
      calStartDate := recPayCalendar.ptrcaln_start_date;
      calEndDate := recPayCalendar.ptrcaln_end_date;

      CLOSE curPayCalendar;


      dbms_output.put_line('Cleaning up tcplus.tcp_bann_transfer table');

      delete from tcapp.hr_tcp_bann_staging;

      commit;

      dbms_output.put_line('Done deleting from table');



      ----PROCESS RECORDS IN EXTERNAL TABLE/FILE

      FOR recEmplHoursTCP in curEmplHoursTCP LOOP
        BEGIN
        ---CONVERT BADGE NUMBER TO TCID AND PIDM
        emplPidm         := null;
        emplTcid         := null;
        emplPosn         := null;
        emplSuff         := null;
        emplEclsCode     := null;
        emplEarnCode     := null;
        emplWorkDate     := null;
        emplHrs          := null;
        emplSalTable     := null;
        emplSalGrade     := null;
        emplSpecialRate  := null;

        OPEN curEmplId(recEmplHoursTCP.badge_number);

        FETCH curEmplId INTO recEmplId;

        IF(curEmplId%NOTFOUND) then
           CLOSE curEmplId;
           RAISE invalid_id;
        end if;

        emplPidm := recEmplId.spriden_pidm;
        emplTcid := recEmplId.spriden_id;

        CLOSE curEmplId;

        --POPULATE WORK DATE

        emplWorkDate := recEmplHoursTCP.date_in;


        ---GET EMPLOYEE'S POSITION,SUFFIX,SAL TABLE, SAL GRADE

        OPEN curEmplJobRec(emplPidm, emplWorkDate);

        FETCH curEmplJobRec INTO recEmplJobRec;

        IF(curEmplJobRec%NOTFOUND) then
           CLOSE curEmplJobRec;
           RAISE invalid_job;
        end if;


        emplPosn := recEmplJobRec.nbrjobs_posn;
        emplSuff := recEmplJobRec.nbrjobs_suff;
        emplEclsCode := recEmplJobRec.nbrjobs_ecls_code;
        emplSalTable := recEmplJobRec.nbrjobs_sal_table;
        emplSalGrade := recEmplJobRec.nbrjobs_sal_grade;

        CLOSE curEmplJobRec;

        ---CONVERT JOB CODE TO BANNER EARN CODE

        OPEN curEarnXwalkLookup(recEmplHoursTCP.job_code);

        FETCH curEarnXwalkLookup INTO recEarnXwalkLookup;

        IF(curEarnXwalkLookup%NOTFOUND) then
          CLOSE curEarnXwalkLookup;
          RAISE invalid_earn_code;
        end if;


        emplEarnCode := recEarnXwalkLookup.xwalk_earn_code;

        CLOSE curEarnXwalkLookup;

        emplHrs := recEmplHoursTCP.hours;

       ---Get Supervisor rate if supervisor pay
       if (emplEarnCode in ('SPV','SRO')) then

         OPEN curSupervisorXwalkLookup(emplSalTable,emplSalGrade);

         FETCH curSupervisorXwalkLookup INTO recSupervisorXwalkLookup;

         IF(curSupervisorXwalkLookup%NOTFOUND) then
           CLOSE curSupervisorXwalkLookup;
           RAISE invalid_supv_pay;
         end if;




         if (emplEarnCode = 'SPV') then
            if(emplSalGrade = recSupervisorXwalkLookup.xwalk_sal_grade and recSupervisorXwalkLookup.xwalk_supv_grade is null) then
                emplEarnCode := 'REG';
            else
                emplSpecialRate := recSupervisorXwalkLookup.ntrsalb_high;
            end if;
         else
            if(emplSalGrade = recSupervisorXwalkLookup.xwalk_sal_grade and recSupervisorXwalkLookup.xwalk_supv_grade is null) then
                emplEarnCode := 'OT';
            else
                emplSpecialRate := recSupervisorXwalkLookup.ntrsalb_high * 1.5;
            end if;
         end if;

         CLOSE curSupervisorXwalkLookup;

       else
          emplSpecialRate := NULL;

       end if;

       if (emplEarnCode = 'DOC') then

          emplEarnCode := 'UTO';

       end if;

       if (emplEarnCode = 'BKV' and emplEclsCode = '72') then

          emplEarnCode := 'VAC';

       end if;



        INSERT INTO  tcapp.hr_tcp_bann_staging
          (
                  STAGE_YEAR,
                  STAGE_PICT_CODE,
                  STAGE_PAYNO,
                  STAGE_ID,
                  STAGE_POSN,
                  STAGE_SUFF,
                  STAGE_EFFECTIVE_DATE,
                  STAGE_EARN_CODE,
                  STAGE_START_DATE,
                  STAGE_END_DATE,
                  STAGE_HRS,
                  STAGE_SPECIAL_RATE,
                  STAGE_ACTIVITY_DATE
          )
          values
          (
                  calYear,
                  calPictCode,
                  calPayNo,
                  emplTcid,
                  emplPosn,
                  emplSuff,
                  NULL,
                  emplEarnCode,
                  emplWorkDate,
                  emplWorkDate,
                  emplHrs,
                  emplSpecialRate,
                  SYSDATE
           );

      EXCEPTION
          WHEN invalid_id THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message => 'Invalid ID :' || recEmplHoursTCP.badge_number,
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');
          WHEN invalid_job THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message =>  'Invalid JOB - ID:' || emplTcid,
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');
          WHEN invalid_earn_code THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message =>  'Invalid EARN CODE - ID:' || emplTcid,
                          p_package => 'tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');
          WHEN invalid_supv_pay THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message => 'Invalid SUPERVISOR PAY - ID:' || emplTcid,
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');
          WHEN OTHERS THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message => substr(sqlerrm,12),
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');
      END;
      END LOOP;

      ---Populate PHRMTIM with processed records

      FOR recPayrollStaging IN curPayrollStaging LOOP

          insert into phrmtim
          (       PHRMTIM_YEAR,
                  PHRMTIM_PICT_CODE,
                  PHRMTIM_PAYNO,
                  PHRMTIM_ID,
                  PHRMTIM_POSN,
                  PHRMTIM_SUFF,
                  PHRMTIM_EFFECTIVE_DATE,
                  PHRMTIM_EARN_CODE,
                  PHRMTIM_SHIFT,
                  PHRMTIM_BEGIN_DATE,
                  PHRMTIM_END_DATE,
                  PHRMTIM_HRS,
                  PHRMTIM_SPECIAL_RATE,
                  PHRMTIM_ACTIVITY_DATE,
                  PHRMTIM_CHANGE_IND
          )
          values
          (
                  recPayrollStaging.STAGE_YEAR,
                  recPayrollStaging.STAGE_PICT_CODE,
                  recPayrollStaging.STAGE_PAYNO,
                  recPayrollStaging.STAGE_ID,
                  recPayrollStaging.STAGE_POSN,
                  recPayrollStaging.STAGE_SUFF,
                  recPayrollStaging.STAGE_EFFECTIVE_DATE,
                  recPayrollStaging.STAGE_EARN_CODE,
                  '1',
                  recPayrollStaging.STAGE_START_DATE,
                  recPayrollStaging.STAGE_END_DATE,
                  recPayrollStaging.STAGE_HRS,
                  recPayrollStaging.STAGE_SPECIAL_RATE,
                  recPayrollStaging.STAGE_ACTIVITY_DATE,
                  'Y'
          );
          commit;

      END LOOP;
      EXCEPTION
          WHEN invalid_pay_period THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message => 'The pay period entered is invalid',
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');
          WHEN OTHERS THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message => substr(sqlerrm,12),
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');

      END PROCESS_PAYROLL;

      PROCEDURE TRANSFER_LEAVE_HOURS (fileName IN VARCHAR2 DEFAULT NULL)

        IS
        --DECLARE
           delimiter   CONSTANT VARCHAR2 (1) := ',';
           quotes   CONSTANT VARCHAR2 (1) := '"';
           outputLine           VARCHAR2 (32767);
        
           filehandler          utl_file.file_type;
        
           v_procedure util_errorlog.procedure_name%type := 'TRANSFER_LEAVE_HOURS';
           v_location util_errorlog.location%TYPE;
        
        CURSOR curAccruals IS
         SELECT DISTINCT TO_NUMBER(substr(spriden_id,  2, 9 ) ) AS TCP_EMPLOYEE_ID,
                   tcp_data.ACCBANK AS accrual_bank_id,
                   (perleav_begin_balance + perleav_accrued - perleav_taken) AS hrs_accrued,
                   0 AS hrs_over,
                   0 AS manual_entry,
                   'Imported from Banner' AS note,
                   to_char(TRUNC(sysdate), 'YYYY-MM-DD') AS post_date,
                   0 AS hrs_taken,
                   1 as clear_current_totals
            FROM perleav
                JOIN spriden ON
                    perleav_pidm = spriden_pidm
                AND spriden_change_ind IS NULL
                JOIN pebempl ON pebempl_pidm = spriden_pidm
                JOIN nbrjobs jd ON
                    nbrjobs_effective_date = 
                    (SELECT MAX(nbrjobs_effective_date)
                        FROM nbrjobs
                        WHERE
                                nbrjobs_pidm = jd.nbrjobs_pidm
                            AND nbrjobs_posn = jd.nbrjobs_posn
                            AND nbrjobs_suff = jd.nbrjobs_suff
                            AND nbrjobs_effective_date <= TRUNC(SYSDATE))
                AND (nbrjobs_status = 'A' OR (nbrjobs_effective_date = TRUNC(SYSDATE) AND nbrjobs_status = 'T')) 
                AND nbrjobs_pidm = spriden_pidm
                JOIN nbrbjob ON
                    nbrjobs_pidm = nbrbjob_pidm
                AND nbrbjob_posn = nbrjobs_posn
                AND nbrbjob_suff = nbrjobs_suff
                AND nbrbjob_contract_type = 'P'
                JOIN tcapp.hr_tcp_bann_leav_job_xwalk ON perleav_leav_code = hr_tcp_bann_leav_job_xwalk.leave_code
                JOIN ALL_EMPL@timeclock70.teachers tcp_emp
                    ON substr(spriden_id, 2, 9) = tcp_emp.EMPL_ID
                JOIN JOB_ACCRL_XWALK@timeclock70.teachers tcp_data
                    ON tcp_data.JOBCODE= hr_tcp_bann_leav_job_xwalk.job_code
            WHERE
                    pebempl_ecls_code IN ('70','71','72')
                AND perleav_leav_code NOT IN ('ADDL','MISC','JURY','BERE','MILI')
            ORDER BY TCP_EMPLOYEE_ID, POST_DATE;
        --
        BEGIN
        
           IF fileName IS NOT NULL
           THEN
              fileHandler := UTL_FILE.fopen ('PLSQL_OUTPUT', fileName, 'w');
           ELSE
              filehandler :=
                 UTL_FILE.fopen ('PLSQL_OUTPUT', 'timeclockplus_leave_accruals.csv', 'w');
           end if;
        --
        --Headers
        --
              utl_file.put_line (
                                      fileHandler, 
                                      'TCP_EMPLOYEE_ID' || delimiter 
                                      || 'ACCRUAL_BANK_ID' || delimiter 
                                      || 'HRS_ACCRUED' || delimiter 
                                      || 'HRS_OVER' || delimiter 
                                      || 'MANUAL_ENTRY' || delimiter 
                                      || 'NOTE' || delimiter
                                      || 'POST_DATE'  || delimiter
                                      || 'HRS_TAKEN' || delimiter
                                      || 'CLEAR_CURRENT_TOTALS'
                                      , true
                                      );
        --
        --Main Data
        --
           FOR i IN curAccruals
           loop
            begin
              utl_file.put_line (
                                      fileHandler, 
                                      I.tcp_employee_id || delimiter 
                                      || I.accrual_bank_id || delimiter 
                                      || I.hrs_accrued || delimiter 
                                      || i.hrs_over || delimiter 
                                      || I.manual_entry || delimiter 
                                      || quotes || i.note || quotes || delimiter
                                      || I.post_date  || delimiter
                                      || I.hrs_taken || delimiter
                                      || I.clear_current_totals
                                      , TRUE
                                      );
        
                EXCEPTION
        
                WHEN OTHERS THEN
                    util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                                  p_error_message => substr(sqlerrm,12),
                                  p_package => 'hr_tcplus',
                                  p_procedure => v_procedure,
                                  p_location => v_location,
                                  p_module => 'PAYROLL');
                END;
        
              commit;
              
           END LOOP;
        
           UTL_FILE.fclose (fileHandler);
        
        EXCEPTION
        
                WHEN OTHERS THEN
                    util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                                  p_error_message => substr(sqlerrm,12),
                                  p_package => 'hr_tcplus',
                                  p_procedure => v_procedure,
                                  p_location => v_location,
                                  p_module => 'PAYROLL');

      END TRANSFER_LEAVE_HOURS;

 /*     PROCEDURE SYNC_EMPLOYEES IS

      cursor missing_employees IS
        select
        substr(spriden_id,2) as employeeid,
        spriden_last_name as lastname,
        spriden_first_name as firstname,
        pebempl_first_hire_date as datehire,
        nbrjobs_posn || nbrjobs_suff as badgenumber,
        nbrjobs_ecls_code || pebempl_orgn_code_home as employeecode
        from nbrjobs jd,spriden,pebempl
        where nbrjobs_effective_date = (select max(nbrjobs_effective_date)
                                        from nbrjobs
                                        where nbrjobs_pidm = jd.nbrjobs_pidm
                                        and nbrjobs_posn = jd.nbrjobs_posn
                                        and nbrjobs_suff = jd.nbrjobs_suff
                                        and nbrjobs_effective_date <= SYSDATE)
        and (nbrjobs_status = 'A' or
        (nbrjobs_status = 'T' and trunc(nbrjobs_effective_date) = trunc(sysdate)))
        and spriden_pidm = nbrjobs_pidm
        and spriden_change_ind is null
        and nbrjobs_ecls_code in ('70','71','72','78')
        and spriden_pidm = pebempl_pidm
        and not exists (select 1 from csv_EmployeeList@timeclock
                        where csv_EmployeeList."EmployeeId" = substr(spriden_id,2,8));

      BEGIN

       null;

      END SYNC_EMPLOYEES; */


end hr_tcplus;
/