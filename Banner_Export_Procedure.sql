CREATE OR REPLACE PROCEDURE TCAPP.TIMECLOCK_PLUS_LEAVE_ACCRUALS (fileName IN VARCHAR2 DEFAULT NULL)
IS
--DECLARE
   delimiter   CONSTANT VARCHAR2 (1) := ',';
   quotes   CONSTANT VARCHAR2 (1) := '"';
   outputLine           VARCHAR2 (32767);

   filehandler          utl_file.file_type;

   v_procedure util_errorlog.procedure_name%type := 'TRANSFER_LEAVE_HOURS';
   v_location util_errorlog.location%TYPE;

CURSOR curAccruals IS
 SELECT DISTINCT TO_NUMBER(substr(spriden_id,  2, 9 ) ) AS TCP_EMPLOYEE_ID,
           tcp_data.ACCBANK AS accrual_bank_id,
           (perleav_begin_balance + perleav_accrued - perleav_taken) AS hrs_accrued,
           0 AS hrs_over,
           0 AS manual_entry,
           'Imported from Banner' AS note,
           to_char(TRUNC(sysdate), 'YYYY-MM-DD') AS post_date,
           0 AS hrs_taken,
           1 as clear_current_totals
    FROM perleav
        JOIN spriden ON
            perleav_pidm = spriden_pidm
        AND spriden_change_ind IS NULL
        JOIN pebempl ON pebempl_pidm = spriden_pidm
        JOIN nbrjobs jd ON
            nbrjobs_effective_date = 
            (SELECT MAX(nbrjobs_effective_date)
                FROM nbrjobs
                WHERE
                        nbrjobs_pidm = jd.nbrjobs_pidm
                    AND nbrjobs_posn = jd.nbrjobs_posn
                    AND nbrjobs_suff = jd.nbrjobs_suff
                    AND nbrjobs_effective_date <= TRUNC(SYSDATE))
        AND (nbrjobs_status = 'A' OR (nbrjobs_effective_date = TRUNC(SYSDATE) AND nbrjobs_status = 'T')) 
        AND nbrjobs_pidm = spriden_pidm
        JOIN nbrbjob ON
            nbrjobs_pidm = nbrbjob_pidm
        AND nbrbjob_posn = nbrjobs_posn
        AND nbrbjob_suff = nbrjobs_suff
        AND nbrbjob_contract_type = 'P'
        JOIN tcapp.hr_tcp_bann_leav_job_xwalk ON perleav_leav_code = hr_tcp_bann_leav_job_xwalk.leave_code
        JOIN ALL_EMPL@timeclock70.teachers tcp_emp
            ON substr(spriden_id, 2, 9) = tcp_emp.EMPL_ID
        JOIN JOB_ACCRL_XWALK@timeclock70.teachers tcp_data
            ON tcp_data.JOBCODE= hr_tcp_bann_leav_job_xwalk.job_code
    WHERE
            pebempl_ecls_code IN ('70','71','72')
        AND perleav_leav_code NOT IN ('ADDL','MISC','JURY','BERE','MILI')
    ORDER BY TCP_EMPLOYEE_ID, POST_DATE;
--
BEGIN

   IF fileName IS NOT NULL
   THEN
      fileHandler := UTL_FILE.fopen ('PLSQL_OUTPUT', fileName, 'w');
   ELSE
      filehandler :=
         UTL_FILE.fopen ('PLSQL_OUTPUT', 'timeclockplus_leave_accruals.csv', 'w');
   end if;
--
--Headers
--
      utl_file.put_line (
                              fileHandler, 
                              'TCP_EMPLOYEE_ID' || delimiter 
                              || 'ACCRUAL_BANK_ID' || delimiter 
                              || 'HRS_ACCRUED' || delimiter 
                              || 'HRS_OVER' || delimiter 
                              || 'MANUAL_ENTRY' || delimiter 
                              || 'NOTE' || delimiter
                              || 'POST_DATE'  || delimiter
                              || 'HRS_TAKEN' || delimiter
                              || 'CLEAR_CURRENT_TOTALS'
                              , true
                              );
--
--Main Data
--
   FOR i IN curAccruals
   loop
    begin
      utl_file.put_line (
                              fileHandler, 
                              I.tcp_employee_id || delimiter 
                              || I.accrual_bank_id || delimiter 
                              || I.hrs_accrued || delimiter 
                              || i.hrs_over || delimiter 
                              || I.manual_entry || delimiter 
                              || quotes || i.note || quotes || delimiter
                              || I.post_date  || delimiter
                              || I.hrs_taken || delimiter
                              || I.clear_current_totals
                              , TRUE
                              );

        EXCEPTION

        WHEN OTHERS THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message => substr(sqlerrm,12),
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');
        END;

      commit;
      
   END LOOP;

   UTL_FILE.fclose (fileHandler);

EXCEPTION

        WHEN OTHERS THEN
            util_errorlogging.log(p_error_code => substr(sqlerrm,1,9),
                          p_error_message => substr(sqlerrm,12),
                          p_package => 'hr_tcplus',
                          p_procedure => v_procedure,
                          p_location => v_location,
                          p_module => 'PAYROLL');

END;
/